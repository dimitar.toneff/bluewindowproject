import { useState } from "react";
import starImg from "../../assets/half-star.png";
import { FaStar } from "react-icons/fa";
import "./styles.css";

const HalfStarRating = ({ initialValue, onChange, array }) => {
  const [rating, setRating] = useState(3);
  const [hover, setHover] = useState(null);

  // const counterArr = [1, 2, 3, 4, 5];
  return (
    <div>
      {[...Array(5)].map((item, index) => {
        const ratingValue = index + 1;
        return (
          <label>
            <input
              type="radio"
              value={ratingValue}
              onClick={() => {
                setRating(ratingValue);
              }}
            />
            <FaStar
              size={30}
              onMouseOver={() => setHover(ratingValue)}
              onMouseOut={() => setHover(null)}
              color={ratingValue <= (hover || rating) ? "#ffc107" : "#e4e5e9"}
            />
          </label>
        );
      })}
      <p className="rating"> {rating}/5</p>
    </div>
  );
};

export default HalfStarRating;
