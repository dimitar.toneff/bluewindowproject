import React from "react";
import "./styles.css";
import circleImg from "../../assets/circle.PNG";
import HalfStarRating from "../StarsRate";

const CircleTab = () => {
  const counterArr = [1, 2, 3, 4, 5];

  return (
    <div className="rowTab">
      <h1>01</h1>
      <img src={circleImg} alt="circle" />
      <div className="colTab">
        <h1>PlayOjo Casino</h1>
        <HalfStarRating array={counterArr} />
      </div>
    </div>
  );
};

export default CircleTab;
