import "./styles.css";

const BonusTab = () => {
  return (
    <div className="wrap">
      <div className="exclusive">
        <p>EXCLUSIVITÉ</p>
      </div>
      <fieldset className="box-container">
        <legend>Bonus</legend>
        <div>
          <h1>250 CA$</h1>
          <p>+ 80 Tours Gratuits</p>
        </div>
      </fieldset>
    </div>
  );
};

export default BonusTab;
