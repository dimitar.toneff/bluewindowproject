import "./styles.css";
import { BsFillCheckCircleFill } from "react-icons/bs";
const ButtonTab = () => {
  return (
    <>
      <div className="row-wrap">
        <div className="col-wrap">
          <div className="wrap-tick-container">
            <div className="tick-container">
              <BsFillCheckCircleFill color="#00AA57" />
              <span className="tick-text">
                <p>Retraits rapides</p>
              </span>
            </div>
            <div className="tick-container">
              <BsFillCheckCircleFill color="#00AA57" />
              <span className="tick-text">
                <p>Enregistrement en douceur</p>
              </span>
            </div>
            <div className="tick-container">
              <BsFillCheckCircleFill color="#00AA57" />
              <span className="tick-text">
                <p>Benefices non imposables</p>
              </span>
            </div>
          </div>
          <div className="col-wrap">
            <div className="button-wrap">
              <button className="button-tick">JOUER</button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ButtonTab;
