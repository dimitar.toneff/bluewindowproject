import './App.css';
import CircleTab from './components/CircleTab';
import BonusTab from './components/BonusTab';
import ButtonTab from './components/ButtonTab';
import { FaLock } from "react-icons/fa";
import { BsFillCheckCircleFill } from "react-icons/bs";



function App() {
  return (
    <div className="container">
  <div className='row-container'>
    <div className='col'>
      <div className='header'>
          <h1>Les MEILLEURS CASINOS EN LIGNE DU MOMENT</h1>
        </div>
        <div className='rowSubtitle'>
        <div className="subtitle-container">
              <span className="subtitle-icons">
                <p>Tous nos casinos sont: </p>
              </span>
              <FaLock color='#00AA57'/>
            </div>
            <div className="subtitle-container">
              <span className="subtitle-icons">
                <p>Autorise: </p>
              </span>
              <BsFillCheckCircleFill color='#00AA57'/>
            </div>
            <div className="subtitle-container">
              <span className="subtitle-icons">
                <p>Revu par nos experts</p>
              </span>
            </div>
        </div>
        <div className='body'>
        <div className='userChoice'>
           <p>user choice</p>
        </div >
        <div className='row'>
          <div className='col'>
          <CircleTab />
          </div>
          <div className='col'>
          <BonusTab />
          </div>
          <div className='col-3'>
          <ButtonTab />
          </div>
        </div>
        <p className='footer-text'><strong>18+</strong>- Minimum bet amount - 100 - Some other important information - Terms&Conditions text -<strong> Terms&ConditionsLink.com </strong></p>
        </div>
      </div>
      
    </div>
      {/* </header> */}
    </div>
  );
}

export default App;
